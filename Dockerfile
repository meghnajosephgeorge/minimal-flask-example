# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
EXPOSE 5000
RUN chmod +x run_app_prod.sh
ENTRYPOINT ["./run_app_prod.sh"]
